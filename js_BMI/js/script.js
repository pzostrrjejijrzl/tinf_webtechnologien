function berechnung()
{
    // Werte der HTML-Forms in Variabe speichern
    let groesse = document.getElementById("groesse").value;
    let gewicht = document.getElementById("gewicht").value;
    
    // BMI berechnen und auf 2 Dezimalstellen runden (= toFixed Mehtode)
    let bmi = (gewicht/((groesse/100)*(groesse/100))).toFixed(2);
    
    //Ausgabe des BMI-Wertes 
    if (!isNaN(bmi))
    {
        document.getElementById("bmiwert").innerHTML = "Ihr BMI beträgt: " + bmi;
    }
    else
    {
        document.getElementById("bmiwert").innerHTML = "Fehlerhafte Eingabe";
    }

    //Skala
    if (bmi < 17.50)
    {
        document.getElementById("skala").innerHTML = "kritisches Untergewicht"
    }
    else if (bmi >= 17.50 && bmi < 21)
    {
        document.getElementById("skala").innerHTML = "Untergewicht"
    }
    else if (bmi >= 21 && bmi < 27)
    {
        document.getElementById("skala").innerHTML = "Normalgewicht"
    }
    else if (bmi >= 27 && bmi < 32)
    {
        document.getElementById("skala").innerHTML = "Leichtes Übergewicht"
    }
    else if (bmi >= 32)
    {
        document.getElementById("skala").innerHTML = "Übergewicht"
    }
    else
    {
        document.getElementById("skala").innerHTML = "keine Berechnung möglich"
    }
    
}