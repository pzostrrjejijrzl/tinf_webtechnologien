function cc1()
{
    document.getElementById("toanimate").style.backgroundColor = "#5dade2";
}

function cc2()
{
    document.getElementById("toanimate").style.backgroundColor = "#bb8fce";
}

function cc3()
{
    document.getElementById("toanimate").style.backgroundColor = "#f4d03f";
}

function cc4()
{
    document.getElementById("toanimate").style.backgroundColor = "#dc7633";
}

function move_up()
{
    let toanimate = document.getElementById("toanimate");
    let aktuellePosition = parseInt(toanimate.style.top) || 0;
    toanimate.style.top = aktuellePosition - 20 + "px";

}

function move_down()
{
    let toanimate = document.getElementById("toanimate");
    let aktuellePosition = parseInt(toanimate.style.top) || 0;
    toanimate.style.top = aktuellePosition + 20 + "px";
}

function move_left()
{
    let toanimate = document.getElementById("toanimate");
    let aktuellePosition = parseInt(toanimate.style.left) || 0;
    toanimate.style.left = aktuellePosition - 20 + "px";
}

function move_right()
{
    let toanimate = document.getElementById("toanimate");
    let aktuellePosition = parseInt(toanimate.style.left) || 0;
    toanimate.style.left = aktuellePosition + 20 + "px";
}