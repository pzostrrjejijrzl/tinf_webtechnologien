class Produkt
{
    constructor(id, name, netprice)
    {
        this.id = id;
        this.name = name;
        this.netprice = netprice;
    }

    inklUSt()
    {
        return (this.netprice * 1.20);
    }
}


function addPS5()
{
    const produkt = new Produkt(1, "Playstation 5", 499);
    document.getElementById("inhaltwk").innerHTML = produkt.name + ", Netto Preis: " + produkt.netprice + "€ , Brutto Preis: " + produkt.inklUSt() + "€"; 
}


function addXBOX()
{
    const produkt = new Produkt(2, "Xbox Series X", 599);
    document.getElementById("inhaltwk2").innerHTML = produkt.name + ", Netto Preis: " + produkt.netprice + "€ , Brutto Preis: " + produkt.inklUSt() + "€"; 
}